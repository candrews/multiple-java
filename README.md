# multiple-java

This project demonstrates https://gitlab.com/gitlab-org/gitlab/-/issues/465387

This project contains 2 simple Java projects: one using maven and one using Gradle.

Expected: Gemnasium should detect all dependencies in both projects and generate one SBOM for each. There should be an SBOM at `gradle/gl-sbom-*.cdx.json` and another at `maven/gl-*.cdx.json`.

Actual: Gemnasium stops when it finds the first project, which happens to be the gradle project in this case, and only generates an SBOM for it at `gradle/gl-sbom-*.cdx.json`. It does not output an SBOM in the `maven` directory. The job output says:

> Selecting gradle for maven because this is the first match

See the pipeline for this project.
